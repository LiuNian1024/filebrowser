import Vue from "vue";
import VueI18n from "vue-i18n";

import zhCN from "./zh-cn.json";

Vue.use(VueI18n);

const removeEmpty = (obj) =>
  Object.keys(obj)
    .filter((k) => obj[k] !== null && obj[k] !== undefined && obj[k] !== "") // Remove undef. and null and empty.string.
    .reduce(
      (newObj, k) =>
        typeof obj[k] === "object"
          ? Object.assign(newObj, { [k]: removeEmpty(obj[k]) }) // Recurse.
          : Object.assign(newObj, { [k]: obj[k] }), // Copy value.
      {}
    );

export const rtlLanguages = ["he", "ar"];

const i18n = new VueI18n({
  locale: "zh-cn",
  fallbackLocale: "zh-cn",
  messages: {
    "zh-cn": removeEmpty(zhCN),
  },
});

export default i18n;
