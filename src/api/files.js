import { removePrefix } from "./utils";
import { fetchData } from "@/api/staticData/files";

export async function fetch(url) {
  url = removePrefix(url);

  // const res = await fetchURL(`/api/resources${url}`, {});

  // let data = await res.json();

  let data = fetchData;
  data.url = `/files${url}`;

  if (data.isDir) {
    if (!data.url.endsWith("/")) data.url += "/";
    data.items = data.items.map((item, index) => {
      item.index = index;
      item.url = `${data.url}${encodeURIComponent(item.name)}`;

      if (item.isDir) {
        item.url += "/";
      }

      return item;
    });
  }

  return data;
}