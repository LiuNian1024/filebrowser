import "whatwg-fetch";
import cssVars from "css-vars-ponyfill";
import store from "@/store";
import i18n from "@/i18n";
import Vue from "@/utils/vue";
import App from "@/views/App";

cssVars();

new Vue({
  el: "#app",
  store,
  i18n,
  template: "<App/>",
  components: { App },
});
