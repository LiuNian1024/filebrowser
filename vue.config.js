module.exports = {
  runtimeCompiler: true,
  publicPath: "/",
  parallel: 2,
  devServer: {
    proxy: {
      "/api": {
        target: "http://127.0.0.1:8080", //代理的接口地址
        changeOrigin: true,
        // pathRewrite: {},
      },
    },
  },
};
